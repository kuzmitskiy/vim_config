runtime! debian.vim
syntax on
filetype on
filetype plugin on
set nocompatible
set autoindent
set smartindent
set showmatch
set number
set hlsearch
set incsearch
set comments=sl:/*,mb:\ *,elx:\ */
set textwidth=120
set splitright
" line for vertical border
set colorcolumn=120
highlight ColorColumn ctermbg=darkgrey
" set UTF-8 coding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
" tabs settings
set tabstop=4
set shiftwidth=4
set expandtab
" colour scheme
augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c,*.hh,*.cc,*.hpp,*.cpp set filetype=c.doxygen
augroup END
" hotkeys
nnoremap <F5> :make<CR>
map <F2> :w!<CR>
map <F8> :nohlsearch<CR>
nnoremap <F3> :YcmCompleter GoToDefinition<CR>
nnoremap <F4> :YcmCompleter GoToDeclaration<CR>
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>""
let g:ycm_collect_identifiers_from_tags_files = 1
augroup project
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c,*.hh,*.cc,*.hpp,*.cpp set filetype=c.doxygen
augroup END
autocmd vimenter * NERDTree
autocmd filetype make set noexpandtab
autocmd filetype make set nocin
map <c-n> :NERDTreeToggle<cr>
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
autocmd BufWritePre * :%s/\s\+$//e
